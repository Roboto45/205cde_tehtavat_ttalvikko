var notes = (function() {
    var list = [];

    return {
        add: function(note) {
            if (note) {
                // Check if only whitespace
                if (!note.replace(/\s/g, '').length) {
                    return false;
                }
                var item = {timestamp: Date.now(), text: note};
                list.push(item);
                return true;
            }
            return false;
        },
        remove: function(index) {
            // Check if list is empty
            if (list.length !== 0){
                list.splice(index, 1);
                return true;
            }
            return false;
        },
        count: function() {
            return list.length;
        },
        list: function() {},
        find: function(str) {
            for(let i=0; i < list.length; i++) {
                if(list[i].text === str){
                    return true;
                }
            }
            return false;
        },
        clear: function() {
            list.splice(0, list.length);
        }
    }
}());