describe('notes module', function () {
    beforeEach(function() {
        notes.clear();
        notes.add('first note');
        notes.add('second note');
        notes.add('third note');
        notes.add('fourth note');
        notes.add('fifth note');
    });

    it("should be able to add a new note", function () {
        expect(notes.add('sixth note')).toBe(true);
        expect(notes.count()).toBe(6);
    });

    it("should ignore blank notes", function () {
        expect(notes.add('')).toBe(false);
        expect(notes.count()).toBe(5);
    });

    it('should ignore notes containing only whitespace', function() {
        expect(notes.add('   ')).toBe(false);
        expect(notes.count()).toBe(5);
    });

    it('should require a string parameter', function() {
        expect(notes.add()).toBe(false);
        expect(notes.count()).toBe(5);
    });

    // TODO Lisää 8 testiä

    // Test 1
    it('TEST 1: should remove a note', function () {
        expect(notes.remove(4)).toBe(true);
        expect(notes.count()).toBe(4);

    });

    // Test 2
    it('TEST 2: Shouldn\'t remove anything', function () {
        expect(notes.count()).toBe(5);
        notes.clear();
        expect(notes.remove(4)).toBe(false);
    });

    // Test 3
    it('TEST 3: should find the given note', function () {
        expect(notes.find('first note')).toBe(true);
    });

    // Test 4
    it('TEST 4: shouldn\'t find the "note"', function () {
        expect(notes.find('note')).toBe(false);
    });

    // Test 5
    it('TEST 5: shouldn\'t find the "nOTe FiRST"', function () {
        expect(notes.find('nOTe FiRST')).toBe(false);
    });

    // Test 6
    it('TEST 6: shouldn\'t find the anything due to empty parameter', function () {
        expect(notes.find()).toBe(false);
    });

    // Test 7
    it('TEST 7: shouldn\'t find a "" string', function () {
        expect(notes.add('')).toBe(false);
        expect(notes.find('')).toBe(false);
    });

    // Test 8
    it('TEST 8: shouldn\'t find a deleted note', function () {
        expect(notes.count()).toBe(5);
        expect(notes.find('second note')).toBe(true);

        notes.remove(1) // [1] == second note
        expect(notes.find('second note')).toBe(false);

    });
});